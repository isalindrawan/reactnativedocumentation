module.exports = {
	arrowParens: 'avoid',
	bracketSameLine: true,
	bracketSpacing: true,
	singleQuote: true,
	trailingComma: 'all',
	jsxBracketSameLine: true,
	jsxSingleQuote: true,
	tabWidth: 2,
	useTabs: false,
	semi: false,
	endOfLine: 'auto',
}
