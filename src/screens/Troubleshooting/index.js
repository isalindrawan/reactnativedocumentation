import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { TroubleshootingScreen } from './modules'
const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='TroubleshootingScreen'
        component={TroubleshootingScreen}
      />
    </Stack.Navigator>
  )
}
