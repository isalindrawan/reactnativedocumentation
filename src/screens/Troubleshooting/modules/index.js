import { CONTENT } from '../../../constants/basics/troubleshooting'
import { Page } from '../../../components'

export const TroubleshootingScreen = ({ navigation }) => {
  return <Page content={CONTENT} />
}
