import React, { useState } from 'react'
import { Text, TextInput, View } from 'react-native'

export const TextInputDemo = () => {
  const [text, setText] = useState('')
  return (
    <View style={{ padding: 10 }}>
      <TextInput
        style={{
          color: '#000',
          height: 40,
          borderColor: '#CCC',
          borderWidth: 1,
          borderRadius: 8,
        }}
        placeholder='Ketik sesuatu untuk diterjemahkan'
        placeholderTextColor={'#AAA'}
        onChangeText={newText => setText(newText)}
        defaultValue={text}
      />
      <Text style={{ color: '#000', padding: 10, fontSize: 42 }}>
        {text
          .split(' ')
          .map(word => word && '🍕')
          .join(' ')}
      </Text>
    </View>
  )
}
