import { CONTENT } from '../../../constants/basics/handlingTextInput'
import { Button, Page } from '../../../components'

export const HandlingTextInputScreen = ({ navigation }) => {
  return (
    <Page content={CONTENT}>
      <Button
        text='Text Input Demo'
        onPress={() => navigation.navigate('TextInputDemo')}
      />
    </Page>
  )
}
