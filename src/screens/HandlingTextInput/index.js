import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { TextInputDemo } from './modules/textInputDemo'
import { HandlingTextInputScreen } from './modules'

const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='HandlingTextInputScreen'
        component={HandlingTextInputScreen}
      />
      <Stack.Screen name='TextInputDemo' component={TextInputDemo} />
    </Stack.Navigator>
  )
}
