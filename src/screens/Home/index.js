import { ScreenFit } from '../../components'
import { Text, View } from 'react-native'
export const Home = () => {
  return (
    <ScreenFit>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ color: '#000' }}>Home Screen</Text>
      </View>
    </ScreenFit>
  )
}
