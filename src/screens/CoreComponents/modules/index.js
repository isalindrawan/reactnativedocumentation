import { CONTENT } from '../../../constants/basics/coreComponents'
import { Button, Page } from '../../../components'

export { ScrollViewDemo } from './scrollViewDemo'

export const CoreComponentsScreen = ({ navigation }) => {
  return (
    <Page content={CONTENT}>
      <Button
        text='Scroll View Demo'
        onPress={() => navigation.navigate('ScrollViewDemo')}
      />
    </Page>
  )
}
