import { ScreenFit } from '../../../components'
import { ScrollView, View, Text, Image, TextInput } from 'react-native'
import { globalStyles } from '../../../styles'

const Content = () => {
  let views = []

  for (let count = 0; count < 10; count++) {
    views.push(
      <View key={count} style={{ flex: 1 }}>
        <Image
          source={{
            uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
          }}
          style={{ width: 200, height: 200 }}
        />
      </View>,
    )
  }

  return views
}
export const ScrollViewDemo = ({ navigation }) => {
  return (
    <ScreenFit>
      <View style={[globalStyles.container]}>
        <ScrollView>
          <Text style={{ textAlign: 'center', color: '#000' }}>
            Demo Scroll View
          </Text>
          <Content />
          <TextInput
            style={{
              color: '#000',
              height: 40,
              borderColor: 'gray',
              borderWidth: 1,
            }}
            placeholder='Coba ketik disini'
            placeholderTextColor={'#AAA'}
          />
        </ScrollView>
      </View>
    </ScreenFit>
  )
}
