import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { CoreComponentsScreen, ScrollViewDemo } from './modules'

const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='CoreComponentsScreen'
        component={CoreComponentsScreen}
      />
      <Stack.Screen name='ScrollViewDemo' component={ScrollViewDemo} />
    </Stack.Navigator>
  )
}
