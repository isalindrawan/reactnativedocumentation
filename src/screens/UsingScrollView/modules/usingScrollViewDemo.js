import React from 'react'

import { Image, ScrollView, Text, View } from 'react-native'
import { globalStyles } from '../../../styles'

const logo = {
  uri: 'https://reactnative.dev/img/tiny_logo.png',
  width: 64,
  height: 64,
}

export const UsingScrollViewDemo = () => (
  <ScrollView>
    <View style={[globalStyles.container]}>
      <Text style={{ fontSize: 16 }}>Scroll me</Text>
      <Image source={logo} />
      <Image source={logo} />
      <Image source={logo} />
      <Text style={{ fontSize: 16 }}>If you like</Text>
      <Image source={logo} />
      <Image source={logo} />
      <Image source={logo} />
      <Text style={{ fontSize: 16 }}>Scrolling down</Text>
      <Image source={logo} />
      <Image source={logo} />
      <Image source={logo} />
      <Text style={{ fontSize: 16 }}>What's the best</Text>
      <Image source={logo} />
      <Image source={logo} />
      <Image source={logo} />
      <Text style={{ fontSize: 16 }}>Framework around?</Text>
      <Image source={logo} />
      <Image source={logo} />
      <Image source={logo} />
      <Text style={{ fontSize: 16 }}>React Native</Text>
    </View>
  </ScrollView>
)
