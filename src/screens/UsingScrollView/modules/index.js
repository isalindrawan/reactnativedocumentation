import { CONTENT } from '../../../constants/basics/usingScrollView'
import { Button, Page } from '../../../components'

export const UsingScrollViewScreen = ({ navigation }) => {
  return (
    <Page content={CONTENT}>
      <Button
        text='Scroll View Demo'
        onPress={() => navigation.navigate('UsingScrollViewDemo')}
      />
    </Page>
  )
}
