import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { UsingScrollViewScreen } from './modules'
import { UsingScrollViewDemo } from './modules/usingScrollViewDemo'
const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='UsingScrollViewScreen'
        component={UsingScrollViewScreen}
      />
      <Stack.Screen
        name='UsingScrollViewDemo'
        component={UsingScrollViewDemo}
      />
    </Stack.Navigator>
  )
}
