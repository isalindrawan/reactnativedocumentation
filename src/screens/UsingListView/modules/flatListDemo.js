import React from 'react'

import { FlatList, StyleSheet, Text, View } from 'react-native'
import { globalStyles } from '../../../styles'

const styles = StyleSheet.create({
  item: {
    padding: 10,
    color: '#000',
    fontSize: 18,
    height: 44,
  },
})

export const FlatListDemo = () => {
  return (
    <View style={{ ...globalStyles.container, alignItems: 'flex-start' }}>
      <FlatList
        data={[
          { key: 'Devin' },
          { key: 'Dan' },
          { key: 'Dominic' },
          { key: 'Jackson' },
          { key: 'James' },
          { key: 'Joel' },
          { key: 'John' },
          { key: 'Jillian' },
          { key: 'Jimmy' },
          { key: 'Julie' },
        ]}
        renderItem={({ item }) => <Text style={styles.item}>{item.key}</Text>}
      />
    </View>
  )
}
