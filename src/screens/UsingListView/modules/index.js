import { CONTENT } from '../../../constants/basics/usingListView'
import { Button, Page } from '../../../components'

export const UsingListViewScreen = ({ navigation }) => {
  return (
    <Page content={CONTENT}>
      <Button
        text='Flat List Demo'
        onPress={() => navigation.navigate('FlatListDemo')}
      />
      <Button
        text='Section List Demo'
        onPress={() => navigation.navigate('SectionListDemo')}
      />
    </Page>
  )
}
