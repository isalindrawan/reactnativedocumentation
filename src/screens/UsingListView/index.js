import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { UsingListViewScreen } from './modules'
import { FlatListDemo } from './modules/flatListDemo'
import { SectionListDemo } from './modules/sectionListDemo'

const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='UsingListViewScreen'
        component={UsingListViewScreen}
      />
      <Stack.Screen name='FlatListDemo' component={FlatListDemo} />
      <Stack.Screen name='SectionListDemo' component={SectionListDemo} />
    </Stack.Navigator>
  )
}
