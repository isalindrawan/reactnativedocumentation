import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { PlatformSpecificCodeScreen } from './modules'
const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='PlatformSpecificCodeScreen'
        component={PlatformSpecificCodeScreen}
      />
    </Stack.Navigator>
  )
}
