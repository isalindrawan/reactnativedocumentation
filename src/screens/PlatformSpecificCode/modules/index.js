import { CONTENT } from '../../../constants/basics/platformSpecificCode'
import { Page } from '../../../components'

export const PlatformSpecificCodeScreen = ({ navigation }) => {
  return <Page content={CONTENT} />
}
