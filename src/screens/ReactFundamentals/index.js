import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { ReactFundamentalsScreen } from './modules'
import { FundamentalsDemo } from './modules/fundamentalsDemo'
const Stack = createNativeStackNavigator()

export default ({ navigation, route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name='ReactFundamentalsScreen'
        component={ReactFundamentalsScreen}
      />
      <Stack.Screen name='FundamentalsDemo' component={FundamentalsDemo} />
    </Stack.Navigator>
  )
}
