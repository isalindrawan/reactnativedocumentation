import { CONTENT } from '../../../constants/basics/reactFundamentals'
import { Button, Page } from '../../../components'

export const ReactFundamentalsScreen = ({ navigation }) => {
  return (
    <Page content={CONTENT}>
      <Button
        style={{ marginTop: 16 }}
        text={'Fundamentals Demo'}
        onPress={() => navigation.navigate('FundamentalsDemo')}
      />
    </Page>
  )
}
