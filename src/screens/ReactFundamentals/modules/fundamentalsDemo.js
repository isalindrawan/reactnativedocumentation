import React, { useState } from 'react'
import { Text, View } from 'react-native'
import { Button } from '../../../components'
import { globalStyles } from '../../../styles'

const Cat = props => {
  const [hungry, setHungry] = useState(true)

  return (
    <View>
      <Text style={{ color: '#000' }}>Hello, I am {props.name} </Text>
      <Button
        text={hungry ? 'Feed me my human slave' : 'Enough for the food'}
        onPress={() =>
          setHungry(prevState => {
            return !prevState
          })
        }
        disabled={!hungry}
      />
    </View>
  )
}

export const FundamentalsDemo = () => {
  return (
    <View style={[globalStyles.container]}>
      <Cat name='Jerry Cantrell' />
      <Cat name='Chris Cornell' />
      <Cat name='Eddie Vedder' />
    </View>
  )
}
