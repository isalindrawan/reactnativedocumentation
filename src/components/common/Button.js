import { Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { buttonStyles } from '../../styles'

export const Button = ({ onPress, text, style, ...props }) => {
  return (
    <TouchableOpacity
      props
      onPress={onPress}
      // spread operator won't work with curly braces, use bracket instead
      // when passing props to style
      style={[buttonStyles.default(props.disabled), style]}
      {...props}>
      <Text style={buttonStyles.text}>{text}</Text>
    </TouchableOpacity>
  )
}
