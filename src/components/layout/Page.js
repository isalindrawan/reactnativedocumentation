import { ScreenFit } from '..'
import { View, ScrollView, StyleSheet } from 'react-native'
import Markdown from 'react-native-markdown-display'
export const Page = ({ content, children }) => {
  return (
    <ScreenFit>
      <ScrollView>
        <View style={[style.container]}>
          <Markdown style={{ body: { color: '#000' } }}>{content}</Markdown>
        </View>
        <View style={style.buttonContainer}>{children}</View>
      </ScrollView>
    </ScreenFit>
  )
}

const style = StyleSheet.create({
  container: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 16,
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
    gap: 16,
  },
})
