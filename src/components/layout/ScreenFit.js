import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context'
import { StatusBar, useColorScheme } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen'

export const ScreenFit = ({ children, style }) => {
  const isDarkMode = useColorScheme() === 'dark'

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : '#FFFFFF',
  }

  const insets = useSafeAreaInsets()
  const safeAreaStyle = {
    height: '100%',
    paddingTop: insets.top,
    paddingBottom: insets.bottom,
    paddingLeft: insets.left,
    paddingRight: insets.right,
    color: '#000000',
  }

  return (
    <SafeAreaView style={[safeAreaStyle, style]}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      {children}
    </SafeAreaView>
  )
}
