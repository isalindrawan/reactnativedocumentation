import { StyleSheet } from 'react-native'

export const globalStyles = StyleSheet.create({
  container: {
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 16,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
