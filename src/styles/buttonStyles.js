import { StyleSheet } from 'react-native'

export const buttonStyles = StyleSheet.create({
  default: disabled => ({
    backgroundColor: disabled ? '#999' : '#000',
    borderRadius: 8,
    padding: 16,
  }),

  text: {
    color: '#FFF',
    fontSize: 14,
    fontFamily: 'OpenSans',
  },
})
