const TITLE = `Using a ScrollView`
const CONTENT = `
**ScrollView** adalah wadah scroll umum yang dapat menampung beberapa component dan view. scrollable item bisa bermacam macam, dan bisa di scroll dan dapat di scroll secara vertical maupun horizontal.
&nbsp;
\`\`\`
import React from 'react';

import {Image, ScrollView, Text} from 'react-native';

const logo = {
	uri: 'https://reactnative.dev/img/tiny_logo.png',
	width: 64,
	height: 64,
};

const App = () => (
  <ScrollView>
    <Text style={{fontSize: 96}}>Scroll me plz</Text>
    <Image source={logo} />
    <Image source={logo} />
    <Image source={logo} />
    <Text style={{fontSize: 96}}>If you like</Text>
    <Image source={logo} />
    <Image source={logo} />
    <Image source={logo} />
    <Text style={{fontSize: 96}}>Scrolling down</Text>
    <Image source={logo} />
    <Image source={logo} />
    <Image source={logo} />
    <Text style={{fontSize: 96}}>What's the best</Text>
    <Image source={logo} />
    <Image source={logo} />
    <Image source={logo} />
    <Text style={{fontSize: 96}}>Framework around?</Text>
    <Image source={logo} />
    <Image source={logo} />
    <Image source={logo} />
    <Text style={{fontSize: 80}}>React Native</Text>
  </ScrollView>
);

export default App;
\`\`\`
&nbsp;
- **ScrollView** dapat diatur untuk memperbolehkan halaman (pagination) melalui view menggunakan gestur usap dengan menambahkan properti *pagingEnabled*. usapan secara horizontal antara view juga bisa di implementasikan pada Android dengan menggunakan **ViewPager** component.
- pada iOS, *ScrollView* dengan 1 item dapat digunakan untuk memperbolehkan pengguna untuk memperbesar konten (**zooming**) dengan menggunakan **maximumZoomScale** dan **minimumZoomScale** properti.
- **ScrollView** sangat baik untuk menampilkan item dengan jumlah yang sedikit dan terbatas. semua element dan view dari **ScrollView** akan dirender. bahkan ketika mereka tidak sedang tampil di layar. jika data atau item.
`
export { TITLE, CONTENT }
