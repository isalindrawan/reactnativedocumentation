const TITLE = `Handling Text Input`
const CONTENT = `
**TextInput** adalah Core Component yang memperbolehkan user untuk memasukkan teks. memiliki prop **onChangeText** yang membutuhkan fungsi untuk dijalankan tiap kali teks berubah. dan prop *onSubmitEditing* yang membutuhkan fungsi untuk dijalankan ketika teks disubmit.

sebagai contoh, semua kata yang diinput oleh user akan berubah menjadi 🍕
\`\`\`
import React, {useState} from 'react';
import {Text, TextInput, View} from 'react-native';

const PizzaTranslator = () => {
	const [text, setText] = useState('');
	return (
		<View style={{padding: 10}}>
			<TextInput
				style={{height: 40}}
				placeholder="Type here to translate!"
				onChangeText={newText => setText(newText)}
				defaultValue={text}
			/>
			<Text style={{padding: 10, fontSize: 42}}>
				{text.split(' ').map(word => word && '🍕').join(' ')}
			</Text>
		</View>
	);
};

export default PizzaTranslator;
\`\`\`
&nbsp;
pada contoh ini, kita menyimpan teks didalam state, karena itu berubah dari waktu ke waktu.
**TextInput** adalah salah satu cara pengguna berinteraksi dengan app.
`

export { TITLE, CONTENT }
