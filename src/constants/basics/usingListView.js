const TITLE = `Using ListView`
const CONTENT = `
React Native menyediakan serangkaian komponen untuk mempresentasikan list data. secara umum, bisa menggunakan **FlatList** atau **SectionList**.

- **FlatList** component menampilkan daftar perubahan yang bergulir, akan tetapi memiliki struktur data yang sama. 
- **FlatList** berkerja dengan baik untuk daftar data yang panjang / banyak, dimana jumlah item dapat berubah setiap saat. 
- **FlatList** hanya merender elemen / item yang sedang ditampilkan di layar, tidak semua elemennya dirender sekaligus, tidak seperti **ScrollList**.
- **FlatList** membutuhkan 2 properti yaitu **data** dan **renderItem**, dimana **data** adalah sumber informasi dari daftar yang akan ditampilkan. sedangkan **renderItem** mengambil 1 sampel data untuk dikembalikan sebagai komponen yang terformart (contoh card dengan layout dan design unik) untuk dirender.

Berikut contoh untuk membuat **FlatList** sederhana dari data yang diharcode. tiap item pada properti **data** akan direneder sebagai komponen **Text** .

\`\`\`
import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const FlatListBasics = () => {
  return (
    <View style={styles.container}>
      <FlatList
        data={[
          {key: 'Devin'},
          {key: 'Dan'},
          {key: 'Dominic'},
          {key: 'Jackson'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]}
        renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
      />
    </View>
  );
};

export default FlatListBasics;
\`\`\`
&nbsp;
Untuk merender serangkaian data yang dipecah menjadi beberapa bagian logis, mungkin dengan judul di tiap bagian, mirip dengan **UITabViews** di **iOS**, maka **SectionList** adalah solusinya.
&nbsp;
\`\`\`
import React from 'react';
import {SectionList, StyleSheet, Text, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const SectionListBasics = () => {
  return (
    <View style={styles.container}>
      <SectionList
        sections={[
          {title: 'D', data: ['Devin', 'Dan', 'Dominic']},
          {
            title: 'J',
            data: [
              'Jackson',
              'James',
              'Jillian',
              'Jimmy',
              'Joel',
              'John',
              'Julie',
            ],
          },
        ]}
        renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
        renderSectionHeader={({section}) => (
          <Text style={styles.sectionHeader}>{section.title}</Text>
        )}
        keyExtractor={item => \`basicListEntry-\$\{item\}\`}
      />
    </View>
  );
};

export default SectionListBasics;
\`\`\`
`

export { TITLE, CONTENT }
