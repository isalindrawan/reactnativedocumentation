const TITLE = `React Fundamentals`
const CONTENT = `
React Native berjalan diatas React, sebuah pustaka sumber terbuka untuk membangun antarmuka dengan JavaScript. Untuk memanfaatkan React Native secara maksimal, ada baiknya untuk mempelajari React terlebih dahulu.

adapun beberapa konsep inti dari React :

1.  **components**
2.  **JSX**
3.  **props**
4.  **state**
&nbsp;
- **Components**  
    Component adalah salah satu konsep inti dari React. component merupakan pondasi untuk membangun antar muka (UI).
    
    - React memperbolehkan untuk membuat component, reusable UI element untuk app.
    - Dalam app React, tiap bagian dari UI adalah component.
    - React component adalah fungsi umum javascript kecuali :  
        a. nama selalu dimulai dengan huruf besar (capital case).  
        b. kembalian berupa JSX markup.
&nbsp;
- **JSX**  
    React dan React Native mengunakan JSX, sebuah syntax yang mengijinkanmu untuk menulis element didalam javascript seperti \`<Text> Hello </Text>\` karena JSX adalah JavaScript, kamu dapat menggunakan variabel didalamnya. berikut contohnya :
    &nbsp;
    \`\`\`
    import React from 'react';
    import {Text} from 'react-native';
    
    const Cat = () => {
        const name = 'Meowie';
        return <Text>Hello, I am {name}</Text>
    };
    
    export default Cat;
    \`\`\`
    &nbsp;
    javascript expression juga akan berkerja diantara kurung kurawal, termasuk pemanggilan fungsi seperti \`{getFullName("John", "The Invincible", "Dillinger")}\`
    &nbsp;
    \`\`\`
    import React from 'react';
    import {Text} from 'react-native';
    
    const getFullName = (firstName, middleName, lastName) => {
        return \`\$\{firstName\} \$\{middleName\} \$\{lastName\}\`
    };
    \`\`\`
&nbsp;
- **Custom Componnets**  
    React memperbolehkan meletakkan / menambahkan components di dalam component lain untuk membuat component baru (bertingkat). component yang dapat digunakan secara bertingkat dan dapat digunakan secara berulang ulang adalah jiwa dari paradigma React. contoh :
    &nbsp;
    - **Custom**
        \`\`\`
        import React from 'react';
        import {Text, TextInput, View} from 'react-native';
        
        const Cat = () => {
            return (
                <View>
                    <Text>Hello, I am... </Text>
                    <TextInput
                        style={{
                        height: 40,
                        borderColor: 'gray',
                        borderWidth: 1
                        }}
                        defaultValue="John Doe"
                    />
                </View>
            );
        };
        
        export default Cat;
        \`\`\`
    &nbsp;    
    - **Multiple Components**   
        \`\`\`
        import React from 'react';
        import {Text, TextInput, View} from 'react-native';
        
        const Cat = () => {
            return (
                <View>
                    <Text>Hello, I am... </Text>
                </View>
            );
        };
        
        const Cafe = () => {
            <View>
                <Cat />
                <Cat />
                <Cat />
                <Cat />
            </View>
        };
        
        export default Cafe;
        \`\`\`
        &nbsp;
        Component apapun yang merender komponen lain disebut **parent component**. disini Cafe adalah parent component dan tiap Cat adalah child component.
&nbsp;
- **Props**  
    Props adalah kependekan dari "properties". props memperbolehkan untuk mengkustomisasi React components, contohnya ketika passing tiap <cat>dengan nama yang berbeda ketika di render :</cat>
    &nbsp;
    \`\`\`
    import React from 'react';
    import {Text, TextInput, View} from 'react-native';
    
    const Cat = props => {
        return (
            <View>
                <Text>Hello, I am {props.name} </Text>
            </View>
        );
    };
    
    const Cafe = () => {
        <View>
            <Cat name="Jerry Cantrell" />
            <Cat name="Chris Cornell" />
            <Cat name="Eddie Vedder" />
        </View>
    };
    
    export default Cafe;
    \`\`\`
    &nbsp;    
- **State**  
    State seperti penyimpan data personal pada sebuah component. state sangat berguna dalam mengatasi data yang berubah dari waktu ke waktu atau yang datang bersamaan dengan interaksi pengguna. state memberikan ingatan pada component.
    &nbsp;
    Contoh berikut bertempat pada cat cafe dimana 2 kucing lapar menunggu untuk diberi makan. rasa lapar mereka, dengan ekspektasi terus berubah dari waktu ke waktu (tidak seperti namanya), disimpan sebagai state. untuk memberi makan kucing, tekan tombol yang akan memperbarui state mereka.
    &nbsp;
    State dapat ditambahkan kedalam component dengan memanggil **React useState Hook**. ***hook adalah sejenis fungsi yang memperbolehkan kita untuk mengaitkan (hook into) fitur React***. sebagai contoh, useState adalah sebuah Hook yang memperbolehkan untuk menambah state kedalam componen yang berjenis fungsional .    
    &nbsp;
    \`\`\`
    import React, {useState} from 'react';
    import {Text, TextInput, View, Button} from 'react-native';
    
    const Cat = props => {
    
        const [hungry, setHungry] = useState(false);
        
        return (
            <View>
                <Text>Hello, I am {props.name} </Text>
                <Button
                    onPress={() => setHungry(prevState => return !prevState)}
                    disabled={!hungry}
                    title={hungry ? 'Feed me my human slave' : 'Enough for the food'}
                />
            </View>
        );
    };
    
    const Cafe = () => {
        <View>
            <Cat name="Jerry Cantrell" />
            <Cat name="Chris Cornell" />
            <Cat name="Eddie Vedder" />
        </View>
    };
    
    export default Cafe;
    \`\`\`
`

export { TITLE, CONTENT }
