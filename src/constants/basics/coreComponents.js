const TITLE = `Core and Native Components`
const CONTENT = `
React Native memiliki banyak Komponen Inti untuk segala hal mulai dari kontrol hingga indikator aktivitas. Anda sebagian besar akan bekerja dengan Komponen Inti berikut:

- **View**
Wadah yang mendukung tata letak dengan flexbox, gaya, beberapa penanganan sentuhan, dan kontrol aksesibilitas.
	- android : \`<ViewGroup>\`
	- react native : \`<View>\`
	- ios : \`<UIView>\`
	- web : \`<div>\`
	&nbsp;
- **Text**
Menampilkan, menata, dan menyusun rangkaian teks dan bahkan menangani peristiwa sentuh.
	- android : \`<TextView>\`
	- react native : \`<Text>\`
	- ios : \`<UITextView>\`
	- web : \`<p>\`
	&nbsp;
- **Image**
Menampilkan berbagai jenis gambar.
	- android : \`<TextView>\`
	- react native : \`<Text>\`
	- ios : \`<UITextView>\`
	- web : \`<p>\`
	&nbsp;

**Example**
\`\`\`
import React from 'react';
import {View, Text, Image, ScrollView, TextInput} from 'react-native';

const App = () => {
  return (
		<ScrollView>
		  <Text>Some text</Text>
		  <View>
			<Text>Some more text</Text>
			<Image
			  source={{
				uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
			  }}
			  style={{width: 200, height: 200}}
			/>
		  </View>
		  <TextInput
			style={{
			  height: 40,
			  borderColor: 'gray',
			  borderWidth: 1,
			}}
			defaultValue="You can type in me"
		  />
		</ScrollView>
	);
};

export default App;

\`\`\`
`

export { TITLE, CONTENT }
