const TITLE = `Platform Specific Code`
const CONTENT = `
Ketika membangun sebuah cross-platform app, sebisamungkin lakukan penggunaan berulang pada codingan. skenario akan muncul dimana sangat masuk akal untuk membuat / menulis kode yang berbeda, sebagai contoh, ketika mengimplementasikan komponen visual yang berbeda untuk **Android** dan **iOS**.

React Native menyediakan 2 cara untuk mengatur code dan memisahkannya berdasarkan jenis platformnya.

1.  menggunakan modul Platform.
2.  menggunakan platform-specific file extensions.
&nbsp;
- **Platform module**
    React Native menyediakan module untuk mendeteksi di platform mana aplikasi berjalan. kamu dapat menggunakan logika deteksi untuk mengimplementasikan platform-specific code. gunakan opsi ini ketika hanya sebagian kecil dari komponen yang membutuhkan platform-specific.
    
    \`\`\` js
    import {Platform, StyleSheet} from 'react-native';
    
    const styles = StyleSheet.create({
        height: Platform.OS === 'ios' ? 200 : 100,
    });
    \`\`\`
    &nbsp;
    Platform.OS akan menjadi ios ketika aplikasi berjalan pada iOS dan android jika berjalan pada Android.
    
    Ada juga method Platform.select yang bisa di passing object yang mana key nya bisa salah satu dari \`'ios' | 'android' | 'native' | 'default'\` yang me-return value yang paling sesuai untuk platform yang sedang berjalan.
    \`\`\` js
    import {Platform, StyleSheet} from 'react-native';
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        ...Platform.select({
          ios: {
                backgroundColor: 'red',
          },
          android: {
                backgroundColor: 'green',
          },
          default: {
          
                // other platforms, web for example
                backgroundColor: 'blue',
          },
        }),
      },
    });
    \`\`\`
    &nbsp;
    Kode diatas akan menghasilkan sebuah container dengan \`flex: 1\` pada semua platform, memiliki warna background merah untuk ios, hijau untuk *android* dan biru untuk platform lain.
&nbsp;    
- **Platform-specific extensions**
    Ketika kode platform-specific semakin kompleks, coba pertimbangkan untuk memisahkan kode di file yang berbeda. React Native akan mendeteksi apakah file memiliki extensi \`.ios\` atau \`.android\` dan me-load file platform yang relevan ketika dibutuhkan oleh komponen lain. sebagai contoh, terdapat 2 file ini dalam project :
	\`\`\`
	BigButton.ios.js
	BigButton.android.js 
	\`\`\`
    &nbsp;
	lalu component dapat di-import sebagai berikut  :
	\`import BigButton from './BigButton'\`
	
	React Native akan secara otomatis mengambil file yang tepat berdasarkan *platform* yang sedang berjalan.
&nbsp;
- **Native-specific extensions (berbagi kode dengan NodeJS dan Web)**
	Extensi \`.native.js\` juga dapat digunakan ketika sebuah modul butuh untuk dapat berbagi antara NodeJS/Web dan React Native tetapi pada android dan ios tidak memiliki perbedaan. hal ini sangat berguna untuk project yang memiliki kesamaan kode yang dibagikan antara React Native dan ReactJS.
	&nbsp;
	Sebagai contoh, terdapat beberapa file seperti berikut dalam project :
	\`\`\`
	container.js
	container.native.js
	\`\`\`
    &nbsp;
	Komponen tersebut dapat di-import tanpa menggunakan extensi \`.native\` sebagai berukut :
	\`\`\`
	import Container from \`./Container\`
	\`\`\`
 
`

export { TITLE, CONTENT }
