const TITLE = `Troubleshooting`
const CONTENT = `
Ada beberapa masalah umum yang mungkin terjadi dalam persiapan / pengembangan dengan *React Native*.

- **Port already in use**  
    Metro Builder berjalan pada port 8081. jika proses lain sudah menggunakan port tersebut, maka proses tersebut harus ditutup (terminate), atau merubah port yang digunakan oleh bundler.
    &nbsp;
    - **Menutup proses yang berjalan pada port 8081**.
        
        \`\`\`
        # pada linux gunakan perintah dibawah untuk mencari
        # proses yang menggunakan port 8081
        sudo lsof -i :8081
        
        # setelah itu terminasi dengan perintah dibawah ini
        kill -9 <PID>
        \`\`\`
        &nbsp;
        untuk windows dapat mencari proses yang menggunakan *port* 8081 menggunakan **Resource Monitor** dan hentikan dengan menggunakan **Task Manager**.
        &nbsp;
    - **Menggunakan port selain 8081**  
        **Bundler** dapat diatur untuk menggunakan *port* lain selain 8081 dengan menggunakan parameter port dari root project yang dijalankan.
        
        \`\`\`
        npm start --port=8088
        
        # or
        
        yarn start --port 8088
        \`\`\`
        &nbsp;
- **NPM locking error**
    Jika mengalami error seperti *npm WARN locking Error: EACCESS* ketika mengguanakn React Native CLI coba jalankan perintah berikut :
    
    \`\`\`
    sudo chown -R $USER ~/.npm
    sudo chown -R $USER /usr/share/local/lib/node_modules
    \`\`\`
    &nbsp;
- **No transports available**  
    React Native mengimplementasikan polyfill untuk WebSockets. polyfill ini di inisialisasikan sebagai bagian dari modul react-native yang disertakan kedalam applikasi melalui \`import react from 'react'\`
    &nbsp;  
    jika menyertakan modul dain yang membutuhkan WebSockets seperti Firebase, pastikan untuk me load / require setelah reac-native.
    
    \`\`\`
    import React from 'react';
    import Firebase from 'firebase';
    \`\`\`
    &nbsp;
- **Shell Command Unresponsive Exception**  
    Jika mengalami masalah ShellCommandUnresponsiveExpection seperti :
    
    \`\`\`
    Execution dailed for task ':app:installDebug'
        com.android.builder.testing.api.DeviceException: com.android.ddmlib.ShellCommandUnresponsiveException
    \`\`\`
    &nbsp;
    coba download versi gradle ke 1.2.3 pada \`android/build.gradle\`
    &nbsp;
- **react-native init hangs** 
    Jika memiliki isu dimana menjalankan \`npx reac-native init\` menyebabkan hang pada sistem. coba jalankan kembali di mode verbose agar menampilkan lebih banyak log dan informasi yang dapat memecahkan isu tersebut.
    
    \`\`\`
    npm run android --verbose
    
    # or
    
    yarn android --verbose
    \`\`\`
    &nbsp;
- **Unable to start reac-native package manager (Linux)**  
    **Case 1: Error**  
    **"code": "ENOSPC", "errno": "ENOSPC"**  
    Isu dikarenakan oleh jumlah direktori inotify (digunakan untuk watchman di linux)dapat memonitor. untuk mengatasinya, jalankan perintah ini di jendela terminal :
    
    \`\`\`
    echo fs.inotify.max_user_watches=582222 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
    \`\`\`
    &nbsp;
    **Error: spawnSync ./gradlew EACCES**  
    Jika mendapati isu ketika mengeksekusi \`npm run android\` atau \`yarn android\` di macOS. yang perlu dilakukan adalah coba jalankan \`sudo chmod +x android/gradlew\` untuk memberikan hak akses untuk eksekusi pada file gradlew
`

export { TITLE, CONTENT }
