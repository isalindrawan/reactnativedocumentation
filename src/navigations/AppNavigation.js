import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createRef } from 'react'
import { StackActions } from '@react-navigation/native'
import { TITLE as coreComponentsTitle } from '../constants/basics/coreComponents'
import { TITLE as reactFundamentalsTitle } from '../constants/basics/reactFundamentals'
import { TITLE as handlingTextInputTitle } from '../constants/basics/handlingTextInput'
import { TITLE as usingScrollViewTitle } from '../constants/basics/usingScrollView'
import { TITLE as usingListViewTitle } from '../constants/basics/usingListView'
import { TITLE as troubleshootingTitle } from '../constants/basics/troubleshooting'
import { TITLE as platformSpecificCodeTitle } from '../constants/basics/platformSpecificCode'
import { Home } from '../screens/Home'
import CoreComponents from '../screens/CoreComponents'
import ReactFundamentals from '../screens/ReactFundamentals'
import HandlingTextInput from '../screens/HandlingTextInput'
import UsingScrollView from '../screens/UsingScrollView'
import UsingListView from '../screens/UsingListView'
import Troubleshooting from '../screens/Troubleshooting'
import PlatformSpecificCode from '../screens/PlatformSpecificCode'

const Stack = createNativeStackNavigator()
const Drawer = createDrawerNavigator()
const reference = createRef()

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name={'Home'} component={Home} />
      <Drawer.Screen name={coreComponentsTitle} component={CoreComponents} />
      <Drawer.Screen
        name={reactFundamentalsTitle}
        component={ReactFundamentals}
      />
      <Drawer.Screen
        name={handlingTextInputTitle}
        component={HandlingTextInput}
      />
      <Drawer.Screen name={usingScrollViewTitle} component={UsingScrollView} />
      <Drawer.Screen name={usingListViewTitle} component={UsingListView} />
      <Drawer.Screen name={troubleshootingTitle} component={Troubleshooting} />
      <Drawer.Screen
        name={platformSpecificCodeTitle}
        component={PlatformSpecificCode}
      />
    </Drawer.Navigator>
  )
}

const AppNavigation = ({ navigation, route }) => {
  return (
    <NavigationContainer ref={reference}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name='HomeScreen' component={DrawerNavigator} />
        {/* <Stack.Screen name='ScrollViewDemo' component={ScrollViewDemo} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigation

export const navigate = (name, params) => {
  reference.current?.dispatch(StackActions.replace(name, params))
}
