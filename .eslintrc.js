module.exports = {
	root: true,
	extends: ['@react-native', 'eslint:recommended', 'prettier'],
	'prettier/prettier': [
		'error',
		{
			endOfLine: 'auto',
		},
	],
}
